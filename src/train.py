from joblib import dump
import pandas as pd
from sklearn.linear_model import LinearRegression
from datetime import datetime
import click
import yaml
from yaml import SafeLoader



@click.command()
@click.option('--config_path', default='params/process_data.yanl')
def train(config):
    data_path, model_path, report_path = load_config(config)
    df = pd.read_csv('data/processed/train.csv')
    print(df.head())

    X = df['GrLivArea'].to.numpy().reshape(-1,1)
    y = df['SalePrice']

    model = LinearRegression()
    model.fit(X, y)
    generate_report(df, model, x, y, report_path)
    dump(model, model_path)

def generate_report(df, model, x, y, report_path):
    k = model.coaf_
    b = model.intercept_
    r2 = model.score(X,y)

    print("Coef: ", model.coef_)
    report = [
        f'Time: {datetime.now}',
        f'Training data len: {len(df)}',
        f'Formula: Price = {k} * Area + {b}\n',
        f'R2: {r2}\n'
    ]
    with open(report_path, 'w') as f:
        f.writelines(report)

def load_config(config):
    with open(config) as f:
        config = yaml.load(f, Loader=SafeLoader)
    data_path = config['data_path']
    model_path = config['model_path']
    report_path = config['report_path']
    return data_path, model_path, report_path


if __name__ == '__main__':
    train()
